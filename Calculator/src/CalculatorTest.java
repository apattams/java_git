import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    Calculator cal = new Calculator();

    @Test
    void add() {
        int sum = cal.add(11,1);
        assertEquals(12,sum);
    }

    @Test
    void subtract() {
        int subtract = cal.subtract(5,7);
        assertEquals(-2,subtract);
    }

    @Test
    void multiply() {
        int multiply = cal.multiply(66,10);
        assertEquals(660,multiply);
    }

    @Test
    void divide() {
        int divide = cal.divide(12,4);
        assertEquals(3,divide);
    }
}