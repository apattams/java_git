import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Person {
    private String name;
    private String role;
    private String skill;

    public Person(String name, String role, String skill) {
        this.name = name;
        this.role = role;
        this.skill = skill;
    }

    public String getName() {
        return name;
    }

    public String getRole() {
        return role;
    }

    public String getSkill() {
        return skill;
    }

    public static long getODISkillsCount(List<Person> personList) {
        return personList.stream().filter(skill -> skill.getSkill().equals("odi")).count();
    }

    public static long getJavaSkillsCount(List<Person> personList) {
        return personList.stream().filter(skill -> skill.getSkill().equals("java")).count();
    }

    public static long getSeleniumSkillsCount(List<Person> personList) {
        return personList.stream().filter(skill -> skill.getSkill().equals("selenium")).count();
    }

    public static HashSet<String> getAllRoleCount(List<Person> personList) {
        HashSet<String> skillsSet = new HashSet<String>();
        for (Person pr : personList) {
            skillsSet.add(pr.getRole());
        }
        return skillsSet;
    }

    public static Map<String, String> getPersonNameWithSkill(List<Person> personList) {
        HashMap<String, String> personAndSkillList = new HashMap<String, String>();
        for (Person i : personList) {
            personAndSkillList.put(i.getName(), i.getSkill());
        }

        Map<String, String> personAndSkillListSorted = new TreeMap<String, String>(personAndSkillList);
        return personAndSkillListSorted;
    }

}
