import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PersonMain {

    public static void main(String[] args) {

        try {
            Scanner readFile = new Scanner(
                    new File("C:\\javatrain\\training_attendees.csv"));
            readFile.next();
            Person personObj = null;
            List<Person> personList = new ArrayList<Person>();
            while (readFile.hasNext()) {
                String[] keyFromLine = readFile.next().split(",");
                personObj = new Person(keyFromLine[0], keyFromLine[1], keyFromLine[2]);
                personList.add(personObj);
            }
            readFile.close();
            System.out.println("Total person count = " + personList.size());
            System.out.println("ODI skills count= " + Person.getJavaSkillsCount(personList));
            System.out.println("Java skills counts= " + Person.getJavaSkillsCount(personList));
            System.out.println("Selenium skills counts= " + Person.getSeleniumSkillsCount(personList));
            System.out.println(Person.getAllRoleCount(personList));
            System.out.println(Person.getPersonNameWithSkill(personList));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

}
