import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;


public class PersonMain {
    public static void main(String[] args) {
        try {
            BufferedReader personDetails = new BufferedReader(new FileReader("C:\\javatrain\\training_attendees.csv"));
            String personLine;
            Person person;

            List<Person> personList = new ArrayList<>();
            while ((personLine = personDetails.readLine()) != null) {
// System.out.println(personLine);
                String[] personFromLine = personLine.split(",");
                person = new Person(personFromLine[0], personFromLine[1], personFromLine[2]);
                personList.add(person);
            }
            System.out.println(getOdiSkillCount(personList));
            System.out.println(getJavaSkillCount(personList));
            System.out.println(getSeleniumSkillCount(personList));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static long getOdiSkillCount(List<Person> personList) {
        return personList.stream().filter(s -> s.getSkill().equals("odi")).count();
    }

    private static long getJavaSkillCount(List<Person> personList) {
        return personList.stream().filter(s -> s.getSkill().equals("java")).count();
    }

    private static long getSeleniumSkillCount(List<Person> personList) {
        return personList.stream().filter(s -> s.getSkill().equals("selenium")).count();
    }
}